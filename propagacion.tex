
\documentclass{beamer}
%\documentclass[handout]{beamer}
 \mode<presentation>{ 
 \usetheme{Antibes}%\usetheme{Warsaw}  
\setbeamercovered{transparent}
%   \usecolortheme{albatross} % for inverted color
% \usecolortheme{wolverine} % way too flashy!
\usecolortheme{beaver}
    %\usefonttheme{structurebold}
    %\usetheme{Berkeley}
    % or ...    \setbeamercovered{transparent}
    % or whatever (possibly just delete it)
 
 } 
%%\mode<handout>
\usepackage[spanish]{babel}
\usepackage[utf8]{inputenc}
\usepackage{times}
\usepackage{url}
\usepackage[T1]{fontenc}
\usepackage{alltt}
\usepackage[normalem]{ulem}
\usepackage{xcolor}

\usepackage{MnSymbol,wasysym}

\newcommand{\Pair}[2]{\texttt{#1}\(\leftrightarrow\)\texttt{#2}}
\newcommand{\pair}[2]{\texttt{#1}\(\to\)\texttt{#2}}
\newcommand{\credit}[1]{\textcolor{gray}{\small [#1]}}

\title[Propagación radio]{La propagación de las ondas de radio: introducción}

\author[M.L.\ Forcada]{Mikel L.\ Forcada (EA5IYL)}

\date[ACRACB, 30/09/2023]{ACRACB, 30 sept.\ 2023\\\includegraphics[scale=0.13]{acracb.jpg}}


\newcommand{\empha}[1]{\emph{#1}\/}


\AtBeginSection[]
{
  \begin{frame}<beamer>{Índice}
    \tableofcontents[currentsection,subsectionstyle=hide]
  \end{frame}
}


\begin{document}

\frame{\maketitle}


\begin{frame}<beamer>{Índice} \tableofcontents[subsectionstyle=hide]
\end{frame}


\section{Las ondas de radio}


\begin{frame}
\frametitle{Las ondas de radio son ondas electromagnéticas}
\begin{itemize}
	\item Son variaciones simultáneas (en el espacio y en el tiempo) del campo eléctrico y del campo magnético.
	\item Estas variaciones están ligadas entre sí por las \empha{ecuaciones de Maxwell}.
	\item Viajan \textbf{por el vacío} (no necesitan materia que las soporte) a la velocidad de la luz, $c=299~792~458$~m/s.
	\item Son producidas por corrientes eléctricas variables en la antena emisora.
	\item Inducen corrientes eléctricas variables en la antena receptora.

\end{itemize}
\end{frame}



\begin{frame}
\frametitle{Las ondas de radio son ondas electromagnéticas}
\begin{columns}
\begin{column}{0.5\textwidth}
Se pueden descomponer en ondas \empha{sinusoidales} con una \empha{amplitud} $A$, una \empha{frecuencia} $f$ y una \empha{fase} $\phi$. 

Por ejemplo, en un punto determinado del espacio, el potencial eléctrico (voltaje) varía con el tiempo así:
\[V(t)=A\sin(2\pi f t + \phi).          \]
\end{column}
\begin{column}{0.5\textwidth}
  \includegraphics[scale=0.3]{wave.png}
  (en el ejemplo, $\phi=0$)
  
  \credit{Imagen de Wikipedia}
\end{column}
\end{columns}
\end{frame}

\begin{frame}
  \frametitle{Las ondas en el vacío}
  \begin{itemize}
  \item Las ondas electromagnéticas en el vacío \textbf{se propagan en línea recta}.
  \item Nada las detiene (pero se atenúan con la distancia).
  \item El espacio interestelar se parece mucho al vacío:
  \begin{itemize}
  \item Recibimos la luz de estrellas extremadamente lejanas.
  \item La NASA se comunica aún con la Voyager 1 a 22.000.000.000~km en 2100, 2300 y 8400~MHz.
  \end{itemize}
  \end{itemize}
\end{frame}


\section{Propagación: las ondas y la materia}

\begin{frame}
  \frametitle{Propagación: las ondas y la materia}
  
\begin{itemize}
\item Los fenómenos que llamamos \empha{propagación} se deben a la interacción de las ondas electromagnéticas con \textbf{la materia}.
\item Por tanto, no es \empha{magia}: ¡es \empha{física}!
\item La física de algunos modos de propagación es muy compleja y no se entiende completamente.
\item Esto hace que a menudo sea difícil predecir cuándo ocurrirá un determinado modo de propagación. 
\end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Propagación: las ondas y la materia: ejemplo 1}
  \begin{itemize}
  \item ``No te copio en 70~cm porque estoy apantallado'': 
    \begin{itemize} 
    \item un trozo
    grande de materia (edificio, montaña) absorbe o refleja en otra
    dirección la onda de 433~MHz.
    \end{itemize}
   
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Propagación: las ondas y la materia: ejemplo 2}
  \begin{itemize}
  \item ``Hoy no he podido hacer un QSO con Chile en 20~m'': 
    \begin{itemize}
    \item la capa
    de materia ionizada que tenía que desviar la onda de radio de
    14~MHz (varias veces) entre EA5 y CE en la ionosfera para salvar la curvatura de
    la tierra no lo hace bien.
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Propagación: las ondas y la materia: ejemplo 3}
  \begin{itemize}
  \item ``¿Cómo es posible que Felip (EC5AGC) y yo nos oigamos siempre bien en 2~m si está la Serra dels Plans (1300 m) justo en medio?'': 
  \begin{itemize}
  \item De alguna manera, precisamente la onda, en lugar de ir recta, se desvía hacia abajo después de rozar un trozo grande de materia sólida: la cresta de Plans.
  \end{itemize}
  \begin{center}
  \includegraphics[scale=0.4]{felip.jpeg}
  \end{center}
  \credit{Imagen calculada con \url{heywhatsthat.com}}
  \end{itemize}
\end{frame}



\begin{frame}
  \frametitle{Propagación: las ondas y la materia: ejemplo 4}
  \begin{itemize}
  \item ``He hecho un QSO en 2~m con Mallorca'': 
    \begin{itemize}
    \item aunque Mallorca está claramente
    tras el horizonte, el aire (materia gaseosa) de la troposfera está
    distribuido de tal manera que ha curvado la onda hacia tierra y ha permitido salvar el
    horizonte.
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Propagación: las ondas y la materia: ejemplo 5}
  \begin{itemize}
  \item ``EA5IHH y yo nos copiamos perfectamente en algunas frecuencias de 2 metros y en otras no''
    \begin{itemize}
    \item Esto se debe a las características del terreno y los obstáculos entre EA5IHH y yo.
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Propagación: las ondas y la materia}
    La materia puede
  \begin{itemize}
  \item absorber (y reemitir en todas las direcciones)
  \item reflejar
  \item refractar
  \end{itemize}
  las ondas de radio.\vfill
  La complejidad de la propagación es una de las cosas que hacen interesante la radioafición.
\end{frame}



\section{Tipos de propagación}
\begin{frame}
  \frametitle{Tipos de propagación}
  [No podremos verlos todos, ni con demasiado detalle]
  \begin{itemize}
  \item A la vista
  \item Efecto del filo de cuchillo
  \item Propagación multicamino
  \item Onda de tierra
  \item Troposférica
  \item Ionosférica
    \begin{itemize}
    \item Capa F
    \item Capa E (esporádica)
    \item Capa D 
    \end{itemize}
  \item Dispersión meteórica
  \item Tierra--Luna--Tierra
  \end{itemize}
\end{frame}

\begin{frame}
\frametitle{Propagación a la vista}

  \begin{itemize}
  \item Emisor y receptor \emph{se ven}, en linea recta
  \item La tierra es curva: límites, pero sorprendentes.
  \begin{equation*}
  d \le \sqrt{(2 R_\mathrm{tierra} + h_1)\times h_1} + \sqrt{(2 R_\mathrm{tierra} + h_2)\times h_2} %\simeq 3{,}6\times \sqrt{h_1+h_2\mbox{(m)}}
  \end{equation*}
  \end{itemize}

  Maigmó (SOTA EA5/AT-008): $h_1=$~1296~m; 
  
  Colativí (SOTA EA7/AL-014): $h_2=$~1386~m; 
  
  $R_\mathrm{tierra}=$~6~371~000~m 
  
  $\Rightarrow$ $d\le$~261~411~m	
  
  \includegraphics[scale=0.5]{maigmo-colativi.jpg}

  \credit{Imagen de \url{https://heywhatsthat.com}.}
\end{frame}

\begin{frame}
  \frametitle{Efecto del filo de cuchillo}
\begin{columns}
\begin{column}{0.4\textwidth}
  \begin{itemize}
  \item En inglés, \empha{knife-edge effect}.
  \item Obstáculos de tamaño similar a la longitud de onda $\lambda$ en la
    cresta de una montaña \empha{difractan} (reemiten) la señal.
  \item En VHF y UHF: \(\lambda\simeq~1~\mathrm{m}\)
  \item La señal se recibe en lo que sería una zona de sombra (ejemplo 3)
  \end{itemize}
\end{column}
\begin{column}{0.6\textwidth}
  \includegraphics[scale=0.5]{knifeedgeeffect.jpg}
  \credit{Imagen de G0ISW, \url{https://www.qsl.net/g0isw/knifeedgeeffect.gif}.}
\end{column}
\end{columns}
\end{frame}


\begin{frame}
\frametitle{Propagación multicamino}
\begin{itemize}
\item En inglés \empha{multipath propagation}.
\item La señal se refleja llega por dos (o más) caminos de longitud diferente.
\item Si llegan \empha{en fase}, se refuerzan; si llegan \empha{en contrafase}, se anulan.
  \begin{center}
  \includegraphics[scale=0.21]{Multipath-interference.jpeg}
  \end{center}
  \credit{Imagen de Stine, J.A. \& Portigal, David. (2004). Spectrum 101: An Introduction to Spectrum Management. 220.} 
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Propagación multicamino}
\begin{itemize}
\item Podemos escucharnos bien en una frecuencia y mucho peor unos kHz más arriba o más abajo  (ejemplo 5):
\begin{itemize}
\item Para la misma configuración de caminos, el desfase depende de la frecuencia
\end{itemize}
\item A veces da lugar a distorsión de audio en FM en VHF y UHF.
\item La mayor parte de los contactos en entornos urbanos en VHF y UHF son el resultado de múltiples caminos y del efecto del filo de cuchillo en edificios.
\end{itemize}
\end{frame}



\begin{frame}
  \frametitle{Onda de tierra}
\begin{columns}
\begin{column}{0.55\textwidth}
  \begin{itemize}
  \item Afecta a ondas largas (MF, 300--3000~kHz; LF, 30--300~kHz; VLF, 3--30~kHz, y frecuencias inferiores).
  \item Propagación similar al efecto del filo de cuchillo: difracción sobre la superficie de la tierra.
  \begin{itemize}
  \item El \empha{filo} es más \empha{romo}: el \empha{obstáculo} es más grande.
  \end{itemize}
  \item Polarización vertical.
  \item Conductividad: mejor sobre el agua que sobre tierra.
  \end{itemize}
\end{column}
\begin{column}{0.45\textwidth}
  \includegraphics[scale=0.27]{groundwave.jpeg}
  \credit{Imagen de Eugene Blanchard (2007), \url{https://www.telecomworld101.com/Intro2dcRev2/}}
\end{column}
\end{columns}
\end{frame}


\begin{frame}
  \frametitle{Propagación troposférica}
  \begin{itemize}
  \item Las ondas interaccionan con el aire en las capas bajas (<15~km) de la atmósfera: la \empha{troposfera}.
  \item El índice de refracción del aire depende de la frecuencia, de la temperatura del aire y de su humedad.
  \item Si se producen capas estratificadas de distinta temperatura o humedad, las ondas se curvan.
  \item Si se curvan hacia abajo (superrefracción), podemos \empha{salvar el horizonte} y llegar adonde no llega la vista (ejemplo~3).
  \item Si se curvan hacia arriba (subrefracción), podemos no llegar.
  \end{itemize}
\end{frame}


\begin{frame}
\frametitle{Propagación troposférica}
\begin{center}
\includegraphics[scale=0.6]{refraction-profiles.jpeg}	
\end{center}
\credit{Imagen de \url{https://vu2nsb.com/} (excelente sección sobre propagación).}
\end{frame}

\begin{frame}
\frametitle{Propagación troposférica}
\begin{itemize}
\item Cuando conducimos en verano vemos ``charcos'' en la carretera caliente a lo lejos.
\item Los exploradores sedientos veían agua en el desierto.
\item La temperatura del aire disminuye con la altura: subrefracción.
\end{itemize}
\begin{center}
\includegraphics[scale=0.4]{mirage.jpeg}
\end{center}
\credit{Imagen de Wikipedia}
\end{frame}


\begin{frame}
\frametitle{Propagación troposférica}
\begin{itemize}
\item Cuando la temperatura aumenta con la altura, la refracción es al revés: la onda se curva hacia abajo.
\item En determinadas situaciones de estratificación de las capas de aire, puede quedar \empha{atrapada} entre dos alturas.
\item El 15 de julio de 2023 se llegaba desde Alicante y desde Cerdeña al repetidor IT9ZYI (430,100~MHz) cerca de Palermo (Sicilia): 1200~km.
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Propagación troposférica}
Usando las predicciones meteororológicas se puede intentar predecir la propagación troposférica:
\begin{itemize}
\item F5LEN: \url{https://tropo.f5len.org/}
\item W.\ Hepburn: \url{https://dxinfocentre.com/tropo_eur.html}
\end{itemize}
\begin{center}
\includegraphics[scale=0.16]{06.jpeg} \includegraphics[scale=0.13]{eur006.jpeg}
\end{center}
\end{frame}

\begin{frame}
\frametitle{Propagación troposférica}
La red APRS (\empha{automatic packet reporting system}) en 144,800~MHz da información interesante sobre la propagación en VHF.
\begin{center}
\includegraphics[scale=0.31]{aprs.jpg}

\credit{Imagen de \url{http://vhf.dxview.org}}
\end{center}
\end{frame}


\begin{frame}
\frametitle{Propagación troposférica}
Existe también la \empha{dispersión troposférica} (\empha{troposcatter}):
\begin{itemize}
\item UHF (300~MHz--3~GHz) y EHF (3~GHz--30~GHz)
\item Usos militares y civiles desde los años cincuenta.
\item Una pequeña fracción de las señales pueden ser dispersadas aleatoriamente.
\item Turbulencias $\to$ irregularidades en la densidad del aire $\to$ dispersión.
\item Se puede usar el modo Q65 de WSJT-X (v.\ > 2.4.0).
\item TX potentes, RX sensibles, y antenas muy direccionales.

\end{itemize}
\begin{center}
\includegraphics[scale=0.12]{troposcatter.jpeg} 

\credit{Imagen de \url{https://johnsonfrancis.org/}}
\end{center}
\end{frame}


\begin{frame}
  \frametitle{Propagación ionosférica}
  \begin{itemize}
  \item Por efecto (fundamentalmente) de la radiación solar, los átomos de las capas altas ($\simeq$ 100 km) de la atmósfera se ionizan (pierden electrones, que quedan sueltos).
  \item El \empha{plasma} resultante actúa de manera similar a una lámina de metal: refleja las ondas de radio.
  \item La reflexión depende de la frecuencia y del ángulo.
  \item La formación de estos plasmas depende fuertemente (ejemplo~2) de la actividad solar (ciclos de 11~años). Algunos parámetros:
  \begin{itemize}
  \item Índice de flujo solar (SFI, radiación recibida en 2,8~GHz).
  \item Número de manchas solares (SSN): sus bordes emiten radiación UV ionizante.f
  \end{itemize}
  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{Propagación ionosférica}
  \includegraphics[scale=0.33]{ssn.png}
  \begin{center}
  \credit{Imagen de \url{https://helioforecast.space/solarcycle}}
  \end{center}
  El SFI se correlaciona con el SSN.
\end{frame}


\begin{frame}
  \frametitle{Propagación ionosférica}
  \begin{columns}
  \begin{column}{0.4\textwidth}
  Fundamentalmente hay tres \empha{capas}:
  \begin{itemize}
  \item La capa D (muy débil de noche, absorbe/refleja $\lesssim 10$~MHz).
  \item La capa E (absorbe/refleja $\lesssim 10$~MHz pero a veces $\gtrsim 10$~MHz.)
  \item La capa F (de día y de noche F$_1$ activa).
  \end{itemize}
\end{column}
\begin{column}{0.6\textwidth}
\begin{center}
  \includegraphics[scale=0.48]{Ionosphere_Layers_en.jpeg}
  
  \includegraphics[scale=0.5]{Ionospheric_layers_from_night_to_day.jpeg}
\end{center}
  \credit{Imágenes de Wikipedia}
\end{column}
\end{columns}
\end{frame}


\begin{frame}
  \frametitle{Propagación ionosférica: La capa D}
  \begin{columns}
  \begin{column}{0.5\textwidth}
  \begin{itemize}
  \item 50--90~km.
  \item De día absorbe y refleja $\lesssim 10$~MHz
  \item Prácticamente desaparece de noche (por eso oímos emisoras distantes en onda media, reflejadas en la capa F).
  \end{itemize}  
\end{column}
\begin{column}{0.5\textwidth}
\begin{center}
  \includegraphics[scale=0.55]{Ionosphere_Layers_en.jpeg}
\end{center}
  \credit{Imagen de Wikipedia}
\end{column}
\end{columns}
\end{frame}



\begin{frame}
  \frametitle{Propagación ionosférica: La capa E}
  \begin{columns}
  \begin{column}{0.5\textwidth}

  \begin{itemize}
  \item 90--150~km.
  \item Normalmente absorbe y refleja $\lesssim 10$~MHz
  \item Esporádicamente (``Es''):
  \begin{itemize}
  \item se forman \empha{nubes} que reflejan señales $\gtrsim 50$~MHz;
  \item La \empha{MUF} (frecuencia máxima útil) sube;
  \item más frecuente hacia el solsticio de verano;
  \item contactos a 2000~km en 144~MHz;
  \item mecanismo no bien conocido.
  \end{itemize}
  \end{itemize}  
\end{column}
\begin{column}{0.5\textwidth}
\begin{center}
  \includegraphics[scale=0.55]{Ionosphere_Layers_en.jpeg}
\end{center}
  \credit{Imagen de Wikipedia.}
\end{column}
\end{columns}
\end{frame}



\begin{frame}
  \frametitle{Propagación: ¿Hay esporádica E?}
  \begin{itemize}
  \item Hay sistemas que avisan de la presencia de propagación debida a la capa esporádica E:
  \begin{itemize}
  \item Por ejemplo, el DX Robot (\url{http://www.gooddx.net/}) (puede mandar correo electrónico). 
  \end{itemize}
  \item Pero lo que hacen es observar los QSO en \empha{clusters} y las señales detectadas por receptores conectados a Internet.
  \item Para estar seguros: lanzar ráfagas de CQ (por ejemplo, en FT8) y ver si alguien las recibe en \url{https://pskreporter.info/}.
  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{Propagación ionosférica: La capa F}
  \begin{columns}
  \begin{column}{0.5\textwidth}
  \begin{itemize}
  \item 150--500~km;
  \item F$_1$ (parte baja) se funde con F$_2$ de noche;
  \item (casi) siempre útil en HF, muy usada en radiodifusión;
  \item Normalmente absorbe y refleja señales hasta ~15~MHz.
  \end{itemize}  
\end{column}
\begin{column}{0.5\textwidth}
\begin{center}
  \includegraphics[scale=0.55]{Ionosphere_Layers_en.jpeg}
\end{center}
  \credit{Imagen de Wikipedia.}
\end{column}
\end{columns}
\end{frame}

\begin{frame}
\frametitle{Propagación ionosférica: la zona de silencio}
Zona de silencio = \emph{skip zone}
\begin{center}
  \includegraphics[scale=0.33]{skip.png}
    
    \credit{Imagen de Wikipedia.}
\end{center}
\end{frame}

\begin{frame}
\frametitle{Propagación ionosférica: la zona de silencio}
Es como cuando un pez mira hacia arriba desde dentro del agua.
\begin{center}
  \includegraphics[scale=0.36]{piscina.png}
    
    \credit{Imagen de \url{https://farside.ph.utexas.edu/teaching/316/lectures/lectures.html}}
\end{center}
\end{frame}


\begin{frame}
\frametitle{Propagación ionosférica: la zona de silencio}
El ángulo crítico depende:
\begin{itemize}
\item de la frecuencia
\item de las condiciones de la ionosfera
\end{itemize}
El ancho de la zona de silencio depende:
\begin{itemize}
\item del ángulo crítico
\item de la altura efectiva de la capa reflectora
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Propagación ionosférica: frecuencia máxima útil}

Frecuencia máxima útil (MUF): frecuencia más alta para llegar de $A$ a $B$ por reflexión ionosférica, independientemente de la potencia. 


Hay webs que estiman mapas de MUF (marcan la MUF en el punto medio de $A$ a $B$): p.ej. \url{dxmaps.com} de EA6VQ.


\begin{center}
  \includegraphics[scale=0.22]{muf.png}
    
    \credit{Imagen de \url{https://www.dh8bqa.de/best-vhf-dx-day-ever/}}
\end{center}
\end{frame}

\begin{frame}
\frametitle{Propagación ionosférica: propagación transecuatorial}
 \begin{columns}
  \begin{column}{0.5\textwidth}

  \begin{itemize}
  \item En inglés TEP.
  \item La capa F2 se densifica a N y S
  del ecuador y a gran altura.
  \item Contactos N--S:
  \begin{itemize} 
  \item en VHF baja (50~MHz)
  \item 5000--8000 km
  \item EA $\leftrightarrow$ ZS, V51, LU\ldots
  \end{itemize}
  \item Irregular, por la tarde, equinoccios.
  \end{itemize} 
\end{column}
\begin{column}{0.5\textwidth}
\begin{center}
  \includegraphics[scale=0.10]{tep.jpg}
\end{center}
  \credit{Imagen de \url{https://k5nd.net}}
\end{column}
\end{columns}

\end{frame}

\begin{frame}
 \frametitle{Propagación ionosférica: lo que se escapa}
 La radiofrecuencia que no es absorbida o reflejada por las capas D, E o F escapa al espacio exterior y nos permite:
 \begin{itemize}
 \item Usar los satélites de radioaficionado:
 \begin{itemize}
 \item Con frecuencias a partir de los 15 MHz.
 \item (típicamente a partir de los 144 MHz)
 \end{itemize}
 \item Hacer QSO por dispersión meteórica:
 \begin{itemize}
 \item Típicamente en 50, 70 o 144 MHz.
 \end{itemize}
 \item Hacer QSO tierra--luna--tierra.
 \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{Dispersión meteórica (`meteor scatter')} 
  \begin{itemize}
  \item Las estelas ionizadas de los meteoros  (bólidos) reflejan las ondas cuando entran en la
    atmósfera.
  \item Rutas muy breves de comunicación ($\lesssim 1$~s, mientras se dispersa la estela).
  \item Siempre disponibles, pero muy potentes en temporadas de \emph{lluvia de meteoros}.
  \end{itemize}
  \begin{center}
  \includegraphics[scale=1]{MS.jpg}
      \end{center}
  \credit{Imagen de \url{https://en.wikipedia.org/wiki/SNOTEL}.}
\end{frame}

\begin{frame}
  \frametitle{Dispersión meteórica}
  \begin{itemize}
  \item Uso civil (EEUU): SNOTEL (red de monitorización de nevadas).
  \item Usos militares (OTAN): COMET, AMBCS.
  \item El CNAF de 2021 (nota~UN-131) asigna la banda de 39,0 a 39,2~MHz (recomendación ERC (00)04 de la CEPT).
  \item Alcances hasta 2.000~km (similar a capa E)
  \item De 10~MHz a 1~GHz (sobre todo en 2, 4 y 6~m)
  \item Modos digitales de mensajes rápidos cortos repetidos: MSK441 y FSK144 (software: WSJT-X, MSHV) 
  \begin{itemize}
  \item Anteriormente, telegrafía a gran velocidad.
  \end{itemize}
  \item Son como \empha{pequeños satélites naturales} pero fugaces.
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Tierra--luna--tierra}
  \begin{center}
  \includegraphics[scale=0.16]{moonbounce.jpg}
  \includegraphics[scale=1.1]{helicoidal.jpeg}
  
    \credit{Imágenes de Wikipedia}
  \end{center}
 
  \begin{itemize}\itemsep 0ex
  \item También llamado \empha{rebote lunar}.
  \item Reflexión en la superficie de la luna.
  \item Permite llegar prácticamente al otro lado de la tierra.
  \item Bandas de 13~cm a 6~m.
  \item Antenas direccionales y grandes potencias.
  \item Modos: CW, JT65 (WSJT-X), incluso SSB.
  \item Retardo de 2,7~s.
  \end{itemize}

\end{frame}



\section{Comentarios finales}
\begin{frame}
  \frametitle{Comentarios finales}
  \begin{itemize}
  \item Las ondas de radio se propagan en el vacío.
  \item Pero interaccionan con la materia: se reflejan, se refractan, se difractan.
  \item La materia: montañas, edificios, la atmósfera, cuerpos celestes\ldots
  \item La interacción depende de la frecuencia.
  \item La propagación no es \empha{magia} pero a veces no se puede predecir fácilmente.
  \item La propagación y sus \empha{caprichos} hacen interesante la radioafición.
  \end{itemize}
\end{frame}


\begin{frame}
\frametitle{Estas diapositivas son libres}
El texto de este trabajo se puede distribuir según los términos de
\begin{itemize}
\item la licencia \empha{Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional}
(\url{http://creativecommons.org/licenses/by-sa/4.0/deed.es_ES}),
\begin{center}
\includegraphics[scale=0.02]{cc-by-sa.jpg}
\end{center}
\item o la Licencia General Pública v.\ 3.0 de GNU: (\url{https://www.gnu.org/licenses/gpl-3.0.ca.html}).
\begin{center}
\includegraphics[scale=0.08]{gplv3.jpg}
\end{center}
\end{itemize}
¡Licencia dual! Escríbanme si quieren el código fuente en \LaTeX: \texttt{mikel.forcada@gmail.com}
\end{frame}

\begin{frame}
Las diapositivas se pueden descargar de


\begin{center}
{\huge \texttt{\textbf{t.ly/4FF0R}}}


\includegraphics[scale=0.6]{qr.png}
\end{center}

\end{frame}


\end{document}



\begin{frame}
  \frametitle{}
\end{frame}




